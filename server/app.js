const cookieParser = require('cookie-parser')
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const express = require('express');
dotenv.config({ path: './config.env' });               //confirmed no problems here to
require('./db/conn');

const app = express();
app.use(express.json());
app.use(cookieParser())

app.use(require('./router/auth'));      //we link the route files 

const PORT = process.env.PORT;

app.get('/about', (req, res) => {
    res.send("Hello About,  About");
})

app.listen(PORT, () => {
    console.log(`Server is running at port ${PORT}`);
})

// app.get('/aboutme', (req, res) => {
//     console.log("Hello Guys, Aboutme");
//     res.send("Hello Guys,  Aboutme");
// })

//middleware
// const middleware = (req, res, next) => {
//     console.log("Hello my middleware");
//     next();
// }
// app.get('/', (req, res) => {
//     res.send("Hello Guys,  Home1");
// })

