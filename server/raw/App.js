import React, { createContext,useReducer } from 'react';
import Navbar from './components/Navbar';
import { Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Login from './components/Login';
import Signup from './components/Signup';
import Admin from './components/Admin';
import Errorpage from './components/Errorpage';
import Logout from './components/Logout';
import {initialState, reducer} from "../src/reducer/UseReducer";
import './App.css';


//1) Context API
export const UserContext = createContext();
const Routing = () => {
  return (
    <Switch>

      <Route exact path="/">
        <Home />
      </Route>

      <Route path="/About">
        <About />
      </Route>

      <Route path="/Contact">
        <Contact />
      </Route>


      <Route path="/Login">
        <Login />
      </Route>

      <Route path="/Signup">
        <Signup />
      </Route>

      <Route path="/Admin">
        <Admin />
      </Route>

      <Route path="/logout">
        <Logout />
      </Route>

      <Route>
        <Errorpage />
      </Route>

    </Switch>
  )
}



export const AdminContext = createContext();
const adminRouting = () => {
  return (
    <Switch>

      <Route exact path="/">
        <Home />
      </Route>

      <Route path="/Admin">
        <Admin />
      </Route>

      <Route path="/logout">
        <Logout />
      </Route>

      <Route>
        <Errorpage />
      </Route>

    </Switch>
  )
}



const App = () => {

  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    

    <>
      <UserContext.Provider value={{ state, dispatch }}>

        <Navbar />
        <Routing />

      </UserContext.Provider>
      <AdminContext.Provider value={{ state, dispatch }}>

        <Navbar />
        <adminRouting/>

      </AdminContext.Provider>
    </>

  )
}

export default App;
