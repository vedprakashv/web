const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authenticate = require("../middleware/authenticate");
require('../db/conn');
const User = require('../model/userSchema');

router.get('/', (req, res) => {
    res.send(`hello world from the server router`);
})

//contact us page

router.post('/contact', authenticate, async (req, res) => {

    try {
        const { name, email, phone, message } = req.body;

        if (!name || !email || !phone || !message) {
            console.log("error in contact form");
            return res.json({ error: "Please Fill The Contact Form" });
        }

        const userContact = await User.findOne({ _id: req.userID });

        if (userContact) {

            const userMessage = await userContact.addMessage(name, email, phone, message);

            await userContact.save();

            res.status(201).json({ message: "user contact succesfully" });
        }

    } catch (error) {
        console.log(error)
    }

})

router.get('/voteMenu', (req, res) => {
    res.send(`voteMenu from the server`);
})


router.get('/LogIn', (req, res) => {
    res.send(`Login from the server`);
})


router.get('/register', (req, res) => {
    res.send(`signup from the server`);
})


//async and await validation and registration

router.post('/register', async (req, res) => {

    const { name, department, section, enNo, password, cpassword, email, phone } = req.body;

    if (!name || !department || !section || !enNo || !password || !cpassword || !email || !phone) {
        return res.status(422).json({ error: "Please Fill fields properly" })
    }


    try {

        const userExist = await User.findOne({ email: email });

        if (userExist) {

            return res.status(422).json({ message: "Email Already Exists" });

        } else if (password != cpassword) {

            return res.status(422).json({ message: "password are not matching" });

        } else {

            const user = new User({ name, department, section, enNo, password, cpassword, email, phone });

            //password and confirm pass data hash

            await user.save();

            res.status(201).json({ message: "user registered succesfully" });
        }

    } catch (err) {
        console.error(err);
    }

});



//login route

router.post('/Login', async (req, res) => {

    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(400).json({ error: "please fill the data" });
        }

        const userLogin = await User.findOne({ email: email });

        // console.log(userLogin);

        if (userLogin) {
            const isMatch = await bcrypt.compare(password, userLogin.password);

            const token = await userLogin.generateAuthToken();
            console.log(token);

            res.cookie("jwtoken", token, {
                expires: new Date(Date.now() + 25892000000),
                httpOnly: true
            });


            if (!isMatch) {
                res.status(400).json({ error: "Invalid Credientials" });
            } else {
                res.json({ message: "user Login successfully" });
            }


        } else {
            res.status(400).json({ error: "Invalid Credientials" });
        }



    } catch (err) {
        console.log(err);
    }
})

router.get('/about', authenticate, (req, res) => {
    console.log(`hello my about`);
    res.send(req.rootUser);
});

//get user data for contact us and home page
router.get('/getdata', authenticate, (req, res) => {
    console.log(`hello my about`);
    res.send(req.rootUser);
})

//Logout ka page

router.get('/logout', authenticate, (req, res) => {
    console.log(`hello my Logout page`);
    res.clearCookie('jwtoken', { path: '/' })
    res.status(200).send('User Logout');
});



module.exports = router;
