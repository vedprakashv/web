import React, { useEffect, useState } from 'react'
import model from './images/candidateImages/model.jpg';
import { useHistory } from "react-router-dom";
import aboutPic from '../components/images/about.png'

const About = () => {

    const history = useHistory();

    const [userData, setUserData] = useState({});


    const callAboutPage = async () => {
        try {
            const res = await fetch('/about', {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json'
                },
                
                credentials: "include"
            });

            const data = await res.json();
            console.log(data);
            setUserData(data);

            if (!res.status === 200) {
                const error = new Error(res.error);
                throw error;
            }

        } catch (err) {
            console.log(err);
            history.push('/Login');
        }
    }

    useEffect(() => {
      callAboutPage();
    }, [])

    return (
        <>
            <div className="container m-t-200  ">
                <form method="GET">
                    <div className="row shadow-lg pt-4 pb-4 rounded">
                        <div className="col-md-4 pb-3">
                            <img className="img-fluid" src={aboutPic} alt="" />
                        </div>
                        <div className="col-md-6 ">
                            <h5 className="text-uppercase">{userData.name}</h5>
                            <hr></hr>

                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>
                                </li>

                            </ul>
                            <div className="tab-content" id="myTabContent">
                                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div className="row mt-3">
                                        <div className="col-md-6">
                                            <label>Name</label>
                                        </div>
                                        <div className="col-md-6">
                                            <p className="text-primary text-capitalize">{userData.name}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-md-6">
                                            <label>Department</label>
                                        </div>
                                        <div className="col-md-6">
                                            <p className="text-primary">{userData.department}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-md-6">
                                            <label>Section</label>
                                        </div>
                                        <div className="col-md-6">
                                            <p className="text-primary">{userData.section}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-md-6">
                                            <label>Enrollment No.</label>
                                        </div>
                                        <div className="col-md-6">
                                            <p className="text-primary">{userData.enNo}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-3" >
                                        <div className="col-md-6">
                                            <label>Email</label>
                                        </div>
                                        <div className="col-md-6">
                                            <p className="text-primary">{userData.email}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-3" >
                                        <div className="col-md-6">
                                            <label>Number</label>
                                        </div>
                                        <div className="col-md-6">
                                            <p className="text-primary">{userData.phone}</p>
                                        </div>
                                    </div>

                                </div>
                                <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                                </div>

                            </div>

                        </div>
                        <div className="col-md-2">
                            <button type="submit" name="btnAddMore" className="btn btn-secondary btn-sm rounded-pill pl-3 pr-3" value="Edit Profile">Edit Profile</button>
                        </div>

                    </div>


                </form>
            </div>

        </>
    )
}

export default About;