const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const authenticate = require('../middleware/authenticate');

require('../db/conn');
const User = require('../model/userSchema');            //CALLING USER STRUCTUR WE ALREADY DEFINED

router.get('/', (req, res) => {
    res.send("Hello Guys, Welcome to Home ROUTER");
})

router.post('/register', async (req, res) => {    //POSTING THE DATA & MAKING CALL BACK FUNCTION ASYNC

    const { name, email, phone, work, password, cpassword } = req.body;  // ASSIGNING REQUESTED DATA TO
    //NEW CONSTANT 

    if (!name || !email || !phone || !work || !password || !cpassword) {       //CHECK ALL VALUES TRUE
        return res.status(422).json({ error: "Plz fill all fields" });     //show messagefor empty field
    }

    try {
        const userExist = await User.findOne({ email: email });   // findones email exist on server and wait

        if (userExist) {                          //condition for already registered
            return res.status(422).json({ error: "Email already registered" })
        }
        else if (password != cpassword) {
            return res.status(422).json({ error: "Password does not match" })
        }
        else {
            //create new user
            const user = new User({ name, email, phone, work, password, cpassword })
            await user.save();
            res.status(201).json({ message: "Successfully registered" });
        }
    }
    catch (error) {
        console.log(error);
    }
    //for the unique id an username    
})

router.post('/signin', async (req, res) => {
    try {
        let token;
        const { email, password } = req.body;
        if (!email || !password) {          //  comparing username and password of an ID
            
            return res.status(400).json({ error: "Please fill all the data" });
        }
        const userLogin = await User.findOne({ email: email });  //Checked yha tk

        if (userLogin) {
            const isMatch = await bcrypt.compare(password, userLogin.password);
                                            // [above is entered pas, res.Password]
            token = await userLogin.generateAuthToken();

            res.cookie("jwtoken", token , {
                expires: new Date(Date.now() + 25892000000), //token exp after 30 days
                httpOnly: true
            });

            if (!isMatch) {
                res.status(404).json({ error: '!Invalid Credentials pass' });
            }
            else {
                res.json({ message: "login Successfully", token });
            }
        }
        else {
            res.status(404).json({ error: '!Invalid Credentials' });
        }

    } catch (error) {
        console.log(error)
    }
})

// About us aka page
router.get('/aboutme',authenticate , (req, res) => {
    console.log("HEloo mu amou tpage")
    res.send(req.rootUser);

})

router.get('/getUserData', authenticate, (req,res)=>{
    console.log("Getting data ")
    res.send(req.rootUser);

})

router.post('/feedback', authenticate, async(req, res) => {
    try{
        const {name,email,phone,message} = req.body;

        if (!name || !email || !phone || !message) {       //CHECK ALL VALUES TRUE
            return res.status(422).json({ error: "Plz fill all fields" });     //show messagefor empty field
        }

        const userContact = await User.findOne({ _id: req.userID })

        if(userContact){

            const userMessage = await userContact.addMessage(name ,email, phone, message)
            await userContact.save();

            res.status(201).json({message:"user Contact succesfully"})
        }
    }catch(error){
        console.log(error)
    }
})

router.get('/logout', (req,res)=> {
    console.log("Logout ka page")
    res.clearCookie("jwtoken", { path:'/'});
    res.status(200).send('user logout')
})
module.exports = router;

//confirmed no problems here





















// router.post('/register', async (req,res) => {

//     const { name, email, phone, work, password, cpassword} = req.body;

//     if(!name || !email || !phone || !work || !password || !cpassword){
//         console.log("Errrorr")
//         return res.status(422).json({error:"Plz fill all fields"});
//     }
//     //for the unique id an username

//     User.findOne({ email:email })
//         .then((userExist)=>{
//             if(userExist){
//                 return res.status(422).json({error: "Email allready registered"})
//             }
//             const user = new User({name, email, phone, work, password, cpassword})

//             user.save().then(()=>{
//                 res.status(201).json({message: "Succesfully registered"});
//             }).catch((error)=>{
//                 res.status(500).json({error: "Failed to registered"});
//             }).catch(error=>{ console.log(error); })
//         })


// })