const jwt = require("jsonwebtoken");
const User = require("../model/userSchema")         //confirmed no prolblem here
require('cookie-parser')
 
const Authenticate = async (req, res, next) => {
    
    const token = req.cookies.jwtoken;
    try {
        console.log("in Authentication")
      
        const verifyToken = jwt.verify(token, process.env.SECRET_KEY);

        const rootUser = await User.findOne({ _id: verifyToken._id, "tokens.token": token });
        if (!rootUser) {
            throw new Error("User not found")
        };
        console.log(token)

        req.token = token;
        req.rootUser = rootUser;
        req.userID = rootUser._id;

        next();

    } catch (err) {
        res.status(401).send('Unauthorized: No token Provided')
        console.log(err)
    }
}
module.exports = Authenticate;