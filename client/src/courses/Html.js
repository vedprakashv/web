import React from 'react'
import { NavLink } from 'react-router-dom'

const Html = () => {
    return (
        <>
            <nav className="navbar sticky-top p-0">
              <NavLink to="/" className="navbar-brand text-light col-md-2 font-weight-bold" >HTML</NavLink>
                <input className="form-control col-md-6 rounded-0 border-0 " type="text" placeholder="Search"
                    aria-label="Search" />
                <ul className="navbar-nav px-3">
                    <li className="nav-item d-flex">
                        <NavLink className="nav-link nav-btn p-1 px-4" to="/">Take Quiz</NavLink>
                        <NavLink className="nav-link nav-btn p-1 px-4 ml-3" to="/">Sign out</NavLink>
                    </li>
                </ul>
            </nav>

            <div className="container-fluid pl-0">
                <div className="row">
                    <nav className="col-md-3 d-none d-md-block bg-light sidebar">
                        <div className="sidebar-sticky mt-1 overflow-auto" id="side-navbar">
                            <ul className="nav flex-column ">
                                <li className="nav-item bg-sider" ><NavLink className="nav-link text-light" to="/html-tutorial.html">HTML Home</NavLink></li>
                                <li className="nav-item" ><NavLink className="nav-link" to="/ht-introduction.html">HTML Introduction</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-editors.html">HTML Editors</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-basics.html">HTML Basics</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-elements.html">HTML Elements</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-attributes.html">HTML Attributes</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-headings.html">HTML Headings</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-paragraphs.html">HTML Paragraphs</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-styles.html">HTML Styles</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-formatting.html">HTML Formatting</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-quotations.html">HTML Quotations</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-comments.html">HTML Comments</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-colors.html">HTML Colors</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-css.html">HTML CSS</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-links.html">HTML Links</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-images.html">HTML Images</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-tables.html">HTML Tables</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-lists.html">HTML Lists</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-block-inline.html">HTML Blocks & Inline</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-id.html">HTML Id</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-iframe.html">HTML Iframe</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-javascripts.html">HTML JavaScript</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-filepaths.html">HTML Filepaths</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-head.html">HTML Head</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-layout.html">HTML Layout</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-responsive.html">HTML Responsive</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-computer-code.html">HTML Computer Code</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-semantics.html">HTML Sematics</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-style-guide.html">HTML Style Guide</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-entities.html">HTML Entities</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-symbols.html">HTML Symbols</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-emojis.html">HTML Emojis</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-charset.html">HTML Charset</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-url-encode.html">HTML URL Encode</NavLink> </li>
                                <li className="nav-item"><NavLink className="nav-link" to="/ht-html-xhtml.html">HTML vs XHTML</NavLink> </li>
                            </ul>
                        </div>
                    </nav>

                    <main role="main" className="col-md-8  ml-sm-auto col-lg-9 pt-3 px-4">
                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 className="h1" id="heading">HTML Home</h1>
                        </div>
                        <br />
                        <div className="container mr-2 d-flex justify-content-between">
                            <button className="btn btn-sm btn-outline-dark rounded-0">Previous</button>
                            <button className="btn btn-sm btn-outline-dark rounded-0">Next</button>
                        </div>

                        <div id="div1" className="mt-4">
                            <div id="jumbo" className="jumbotron d-flex">
                                <img className="container w-25" src="img/html_icon.svg" alt="" />

                                <div className="container text-light">
                                    <p>HTML is the standard markup language for Web pages.</p>
                                    <p>With HTML you can create your own Website.</p>
                                    <p>HTML is easy to learn - You will enjoy it!</p>
                                    <button className="btn btn-light"><NavLink className="" to="library/ht-library/ht-introduction.html">Start learning HTML now</NavLink></button>
                                </div>
                            </div>
                        </div>

                    </main>
            </div>
        </div>
        </>
    )
}

export default Html
