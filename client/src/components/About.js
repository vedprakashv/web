import React from 'react'

const About = () => {
    return (
        <div className="main-about">
            <section className="jumbotron d-flex rounded-0 text-center">
                    <div className="container text-light">
                        <h1 className="display-4 mt-3">Web development tutorials site on the Internet</h1>
                        <p className="lead text-muted">
                            WebTutorials is a platform for web developers,
                            covering all the aspects of web development: HTML Tutorial.
                            CSS Tutorial. JavaScript Tutorial and more.
                        </p>
                    </div>
                </section>

                <div className="text-light">
                    <div className="about-container d-flex">
                            <div className="col-md-4 p-1 ">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                            <div className="col-md-8 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                    </div>
                    <div className="about-container d-flex">
                            <div className="col-md-8 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                            <div className="col-md-4 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                    </div>
                    <div className="about-container d-flex">
                            <div className="col-md-4 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                            <div className="col-md-8 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                    </div>
                    <div className="about-container d-flex">
                            <div className="col-md-8 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                            <div className="col-md-4 p-1">
                                <div className="container-bg">
                                    <p className="container-text">hi</p>
                                </div>
                            </div>
                    </div>
                </div>
        </div>
    )
}

export default About
