import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './styles.css';

const Home = () => {
    return (
        <div>
            <main role="main">
                <section className="jumbotron mb-5 rounded-0 text-center">
                    <div className="container text-light">
                        <h1 className="display-4 mt-3">WebTutorials</h1>
                        <p className="lead text-muted">
                            WebTutorials is a platform for web developers,
                            covering all the aspects of web development: HTML Tutorial.
                            CSS Tutorial. JavaScript Tutorial and more.
                        </p>
                        <p>
                            <a href="/Signin" className="btn btn-outline-light  border-light my-2 px-4 shadow">Login</a>
                            <a href="/Signup" className="btn btn-outline-light  border-light my-2 ml-2 px-3 shadow">Sign up</a>
                        </p>
                    </div>
                </section>

                <div className="album py-5 bg-light">
                    <div className="container">

                        <div className="row">
                            <div className="col-md-4">
                                <div className="card shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">HTML</p>
                                    <div className="card-body">
                                        <p className="card-text lead">HTML is the standard markup language for creating Web pages.
                                </p>

                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample1">
                                                <div className="card-body text-light lead">
                                                    HTML is a markup language.
                                        </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <a href="html-tutorial.html" type="button"
                                                    className="btn btn-sm btn-outline-light">Start HTML</a>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample1"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Read
                                        More..</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">CSS</p>
                                    <div className="card-body">
                                        <p className="card-text lead">CSS stands for Cascading Style Sheets.</p>

                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample2">
                                                <div className="card-body text-light lead">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id voluptate
                                                    dolore ratione in provident. Adipisci ipsa delectus ducimus ea voluptate
                                                    libero ad quidem molestias debitis. Adipisci consequatur odio similique
                                                    minus?
                                        </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start CSS</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample2"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample2">Read
                                        More..</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">Bootstrap</p>
                                    <div className="card-body">
                                        <p className="card-text lead">Bootstrap is a free front-end framework.</p>

                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample3">
                                                <div className="card-body text-light lead">
                                                    for faster and easier web development.
                                        </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start
                                            Bootstrap</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample3"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample3">Read
                                        More..</a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">.JS</p>
                                    <div className="card-body">
                                        <p className="card-text lead">JavaScript is the Programming Language for the Web. </p>
                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample4">
                                                <div className="card-body text-light lead">
                                                    JavaScript can update and change both HTML and CSS.
                                        </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start
                                            JavaScript</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample4"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample4">Read
                                        More..</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">jQuery</p>
                                    <div className="card-body">
                                        <p className="card-text lead">jQuery is a JavaScript Library that jQuery greatly simplifies
                                    JS.</p>
                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample5">
                                                <div className="card-body text-light lead">
                                                    jQuery is easy to learn. </div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">jQuery</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample5"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample5">Read
                                        More..</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 ">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">Node.js</p>


                                    <div className="card-body">
                                        <p className="card-text lead">Node.js is an open source server environment.</p>
                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample6">
                                                <div className="card-body text-light lead">
                                                    Node.js allows you to run JavaScript on the server. </div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start
                                            Node.js</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample6"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample6">Read
                                        More..</a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">PHP</p>
                                    <div className="card-body">
                                        <p className="card-text lead">PHP is a server side scripting language.</p>

                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample7">
                                                <div className="card-body text-light lead">
                                                    A powerful tool for making dynamic and interactive Web pages.
                                        </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start PHP</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample7"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample7">Read
                                        More..</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">React</p>
                                    <div className="card-body">
                                        <p className="card-text lead">React is a JavaScript library for building user interfaces.
                                </p>

                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample8">
                                                <div className="card-body text-light lead">
                                                    React is used to build single page applications.
                                        </div>
                                            </div>
                                        </div>

                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start React</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample8"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample8">Read
                                        More..</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="card mb-4 shadow-sm text-center">
                                    <p className="display-4 mt-3" x="50%" y="50%" fill="#eceeef" dy=".3em">Angular.js</p>
                                    <div className="card-body">
                                        <p className="card-text lead">AngularJS extends HTML with new attributes.</p>
                                        <div className="">
                                            <div className="collapse multi-collapse" id="multiCollapseExample9">
                                                <div className="card-body text-light lead">
                                                    AngularJS is perfect for Single Page Applications (SPAs).
                                            </div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-sm btn-outline-light">Start
                                            Angular.js</button>
                                            </div>
                                            <a className=" text-light" data-toggle="collapse" href="#multiCollapseExample9"
                                                role="button" aria-expanded="false" aria-controls="multiCollapseExample9">Read
                                        More..</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </main>

        </div>
    )
}

export default Home
