import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { NavLink } from 'react-router-dom'

const Courses = () => {
    return (
        <>
        
            <div className="main-courses">
                <div className="d-flex justify-content-center p-1">
                    <div className="col-md-6">
                        <NavLink to="../courses/Html" >
                            <div className="box-blue">
                                <p className="display-4">HTML</p>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-white ">
                                <p className="display-4">CSS</p>
                            </div>
                        </NavLink>
                    </div>
                </div>
                <div className="d-flex justify-content-center p-1">
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-white ">
                                <p className="display-4">Javascript</p>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-blue">
                                <p className="display-4">Bootstrap</p>
                            </div></NavLink>
                    </div>

                </div>
                <div className="d-flex justify-content-center p-1">
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-blue">
                                <p className="display-4">React Js</p>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-white ">
                                <p className="display-4">Angular Js</p>
                            </div>
                        </NavLink>
                    </div>

                </div>
                <div className="d-flex justify-content-center p-1">
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-white ">
                                <p className="display-4">MongoDB</p>
                            </div>
                        </NavLink>
                    </div>
                    <div className="col-md-6">
                        <NavLink to="/" >
                            <div className="box-blue">
                                <p className="display-4">PHP</p>
                            </div>
                        </NavLink>
                    </div>

                </div>
            </div>
        </>

    )
}

export default Courses
