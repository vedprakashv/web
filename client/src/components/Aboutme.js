import React, { useEffect, useState } from 'react'
// import { NavLink } from 'react-router-dom'
import image from './img/html_icon.svg'
import './styles.css'
import { useHistory  } from 'react-router-dom'

const Aboutme = () => {
    const history = useHistory(); 
    const [userData,setUserData] = useState();
    console.log("Aboutme Error 1")
    const callAboutPage = async () => {
        console.log("Aboutme Error 2")
        try {

            console.log("Aboutme Error 3")
            const res = await fetch('/aboutme', {
                method: "GET",
                headers: {
                    Accept: "appllication/json"
                    , "Content-Type": "application/json"
                },
                credentials: "include",
            })
            const data = await res.json();
            console.log(data); 
            setUserData(data);

            if (!res.status === 200 || !data) {
                console.log("Aboutme Error 4")
                const error = new Error(res.error);
                throw error;
            }
        } catch (err) {
            console.log("Aboutme Error 5")
            console.log(err)

            history.push('/signin');
        }
    }
    useEffect( () => { 
        callAboutPage(); 
    });


    return (
        <>
            <div id="dashboard">
                <div className="container-fluid pl-0">
                    <div className="row">
                        <nav className="col-md-3 d-none d-md-block sidebar">
                            <div className="sidebar-sticky overflow-auto" id="side-navbar">
                                <ul className="nav nav-tabs flex-column " role="tablist">
                                    <li className="nav-item bg-sider" ><a className="nav-link" id="profile" data-toggle="tab" href="#home ">My Profile </a></li>
                                    <li className="nav-item" ><a className="nav-link" id="profile" data-toggle="tab" href="#editProfile">Edit Profile</a></li>
                                    <li className="nav-item"><a className="nav-link" id="profile" data-toggle="tab" href="#timeLine">TimeLine</a></li>
                                    <li className="nav-item"><a className="nav-link" id="profile" data-toggle="tab" href="/">Downloads</a></li>
                                    <li className="nav-item"><a className="nav-link" id="profile" data-toggle="tab" href="/">Quiz</a> </li>

                                </ul>
                            </div>
                        </nav>
                        <div class="tab-content col-9">
                            <div class="tab-pane container active" id="home">
                                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <h1 className="h3" id="heading">My Profile</h1>
                                </div>
                                <div id="div1" className="mt-1">
                                    <div id="jumbo" className="jumbotron d-flex p-5">
                                        <div className="justify-centent-center align-items-center w-25">
                                            <img className="profile-img" src={image} alt="" />
                                        </div>

                                        <div className="container text-light">
                                            <form method="GET">
                                                <div className="row">
                                                    <div className="col-md-10 offset-2 p-3">
                                                        {/* {/* <h3>{userData.name}</h3> 
                                                        <h6>{userData.work}</h6> */}
                                                        <p>Ranking: 1/10</p>
                                                    </div>
                                                </div>
                                                <div >
                                                    <div className="row" id="general-information">
                                                        <div className="col-md-4 offset-2 ">
                                                            <p>UserID</p>
                                                            <p>Name</p>
                                                            <p>Email</p>
                                                            <p>Phone</p>
                                                            <p>Profession</p>
                                                        </div>
                                                        <div className="col-md-1">
                                                            <p>:</p>
                                                            <p>:</p>
                                                            <p>:</p>
                                                            <p>:</p>
                                                            <p>:</p>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <p>321321321</p>
                                                            {/* <p>{userData.name}</p>
                                                            <p>{userData.email}</p>
                                                            <p>{userData.phone}</p>
                                                            <p>{userData.work}</p> */}
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane container fade" id="editProfile"> <h1>Edit Profile</h1></div>

                            <div class="tab-pane container fade" id="timeLine">
                                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <h1 className="h3" id="heading">Timeline</h1>
                                </div>
                                <div id="div1" className="mt-1">
                                    <div id="jumbo" className="jumbotron d-flex p-5">
                                        <div className="container text-light">
                                            <form>
                                                <div className="justify-centent-center text-center px-4 align-items-center">
                                                    <img className="profile-img w-25" src={image} alt="" />
                                                </div>
                                                <br />
                                                <div >
                                                    <div className="row" id="general-information">
                                                        <div className="col-md-4 offset-2 ">
                                                            <p>Exeperience</p>
                                                            <p>Hoyrly Rate</p>
                                                            <p>Total Projects</p>
                                                            <p>English Level</p>
                                                            <p>Availability</p>
                                                        </div>
                                                        <div className="col-md-1">
                                                            <p>:</p>
                                                            <p>:</p>
                                                            <p>:</p>
                                                            <p>:</p>
                                                            <p>:</p>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <p>Expert</p>
                                                            <p>10$/hr</p>
                                                            <p>23</p>
                                                            <p>Medium</p>
                                                            <p>6month</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane container fade" id="downloads">Downloads</div>
                            <div class="tab-pane container fade" id="quiz">Quiz</div>
                        </div>


                    </div>




                </div>
            </div>

        </>
    )
}

export default Aboutme
