import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import './styles.css'
import { IoCodeSlash, IoSearchCircleOutline } from "react-icons/io5";
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <header>
            <div className="navbar navbar-light bg-light shadow-sm">
                <div className="container d-flex justify-content-between">
                    <NavLink to="/" className="navbar-brand d-flex align-items-center">
                        <IoCodeSlash size={34} color={'black'} /><span className="title pl-1"> WebTutorials</span>
                    </NavLink>
                    <button className="navbar-toggler rounded border-0" type="button" data-toggle="collapse" data-target="#navbarHeader"
                        aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                        <span className=" "><IoSearchCircleOutline size={32} color={'black'} /></span>
                    </button>
                </div>
            </div>
            <div className="collapse bg-light" id="navbarHeader">
                <div className="container py-3">
                    <form className="form row my-lg-0 col-md-8 offset-2 py-3 my-1">
                        <input className="form-control col-md-12 mr-sm-2 my-3 border shadow" type="search" placeholder="Search"
                            aria-label="Search" />
                        <button className="btn btn-outline-primary shadow " type="submit">Search</button>
                    </form>
                </div>
            </div>

            {/* <div className="navigations ">
                <ul className="d-flex list-unstyled">
                    <li><NavLink to="/">Home</NavLink></li>
                    <li><NavLink to="/">Courses</NavLink></li>
                    <li><NavLink to="/">About</NavLink></li>
                </ul>
            </div> */}
            <nav className="navbar navigations navbar-expand-lg navbar-light mb-1">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav list-unstyled">
                        <li className="nav-item active">
                            <NavLink className="nav-link" to="/">Home <span className="sr-only">(current)</span></NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link " to="/Courses">Courses </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/About">About</NavLink>
                        </li>
                        
                    </ul>

                    {/* <div className="ml-auto">
                        <ul className="navbar-nav nav-btn list-unstyled d-flex  ">
                                <li><NavLink to="/Signin" className="btn btn-outline btn-sm px-4 shadow">Login</NavLink></li>
                                <li><NavLink to="/Signup" className="btn btn-outline btn-sm ml-2 px-3 shadow">Sign up</NavLink></li>
                        </ul>
                    </div> */}
                </div>
            </nav>
        </header>
    )
}

export default Navbar
