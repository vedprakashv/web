import React, { useState } from 'react';
import { FaUser } from 'react-icons/fa'
import './styles.css';
import { useHistory } from 'react-router-dom';


const Signup = () => {

    const history = useHistory();
    const [user, setUser] = useState({
        name: "", email: "", phone: "", work: "", password: "", cpassword: ""  
        // Now we have to send these to tht server
    });

    let name, value;
    const handleInputs = (e) => {
        name = e.target.name
        value = e.target.value

        setUser({...user, [name]:value})
    }   

    const PostData = async(e) =>{
        e.preventDefault();

        const { name, email, phone, work, password, cpassword  } = user;

        const res =  await fetch("/register", {
            method:"POST",
            headers:{
                "Content-Type":"application/json"
            },
            body:JSON.stringify({
                name, email, phone, work, password, cpassword
            })
        })
        
        const data = await res.json();

        if(data.status === 422 || !data)
        {
            window.alert("Invalid reg");
            console.log("Invalid reg");
        }
        else{
            window.alert("Regis success");
            console.log("Regis success");

            history.push("/Signin")
        }
    }

    return (
        <>
            <section className="frame">
                <div className="bg-circle1 bg-circle"></div>
                <div className="bg-circle1_1 bg-circle"></div>
                <div className="sm-circle1 bg-circle"></div>
                <div className="bg-circle2 bg-circle"></div>
                <div className="sm-circle2 bg-circle"></div>

                <div className="">
                    <div className="signup-frame">

                        <form method="POST">

                            <div className="container40 m-auto">
                                <div className="signin-user-icon text-center"><FaUser size={80} /> </div>
                                <h1 className="p-3 text-light fw-normal text-center">Register</h1>
                                <div>
                                    <input type="text"  className="form-control form-group" id="name" 
                                        name="name"
                                        value={user.name}
                                        onChange={handleInputs}
                                        placeholder="Username" />
                                </div>
                                <br />
                                <div>
                                    <input type="email" 
                                        className="form-control form-group"
                                        id="email"
                                        name="email"
                                        value={user.email}
                                        onChange={handleInputs}
                                        placeholder="E-mail" />
                                </div>
                                <br />
                                <div>
                                    <input type="text" 
                                        className="form-control form-group"
                                        id="phone"
                                        name="phone"
                                        value={user.phone}
                                        onChange={handleInputs}
                                        placeholder="Phone" />
                                </div>
                                <br />
                                <div>
                                    <input type="text" 
                                        className="form-control form-group"
                                        id="work"
                                        name="work"
                                        value={user.work}
                                        onChange={handleInputs}
                                        placeholder="Work" />
                                </div>
                                <br />
                                <div>
                                    <input type="password" 
                                        className="form-control form-group"
                                        id="password"
                                        name="password"
                                        value={user.password}
                                        onChange={handleInputs}
                                        placeholder="Password" />
                                </div>

                                <br />
                                <div>
                                    <input type="password" 
                                        className="form-control form-group"
                                        id="cpassword"
                                        name="cpassword"
                                        value={user.cpassword}
                                        onChange={handleInputs}
                                        placeholder="Confirm Password" />
                                </div>

                                <div className="checkbox text-light text-muted  pb-3 pt-3">
                                    <label >
                                        <input type="checkbox" value="remember-me" /> Agree to Terms and Conditions
                                    </label>
                                </div>
                                <button 
                                    className="signin-btn w-100 login-btn btn btn-lg container "
                                    type="submit"
                                    name="signup"
                                    id="signup"
                                    value="register"
                                    onClick={PostData}
                                 >Signup</button>
                                <div className="pb-4"></div>
                            </div>

                        </form>
                    </div>

                </div>
            </section>
        </>
    )
}

export default Signup
