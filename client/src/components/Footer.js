import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import './styles.css';

const Footer = () => {
    return (
        <div>
            <footer className="text-muted">
                <div className="container py-5">
                    <div className="row">
                        <div className="col-12 col-md">
                            <i className="fa fa-pencil-square-o fa-4x"></i>
                            <small className="d-block mb-3 text-muted">&copy; 2017-2018</small>
                        </div>
                        <div className="col-6 col-md">
                            <h5>Features</h5>
                            <ul className="list-unstyled text-small">
                                <li><a href="/" className="text-muted" >Cool stuff</a></li>
                                <li><a href="/" className="text-muted" >Random feature</a></li>
                                <li><a href="/" className="text-muted" >Team feature</a></li>
                                <li><a href="/" className="text-muted" >Stuff for developers</a></li>
                                <li><a href="/" className="text-muted" >Another one</a></li>
                                <li><a href="/" className="text-muted" >Last time</a></li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <h5>Resources</h5>
                            <ul className="list-unstyled text-small">
                                <li><a href="/" className="text-muted" >Resource</a></li>
                                <li><a href="/" className="text-muted" >Resource name</a></li>
                                <li><a href="/" className="text-muted" >Another resource</a></li>
                                <li><a href="/" className="text-muted" >Final resource</a></li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <h5>Resources</h5>
                            <ul className="list-unstyled text-small">
                                <li><a href="/" className="text-muted" >Business</a></li>
                                <li><a href="/" className="text-muted" >Education</a></li>
                                <li><a href="/" className="text-muted" >Government</a></li>
                                <li><a href="/" className="text-muted" >Gaming</a></li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <h5>About</h5>
                            <ul className="list-unstyled text-small">
                                <li><a href="/" className="text-muted" >Team</a></li>
                                <li><a href="/" className="text-muted" >Locations</a></li>
                                <li><a href="/" className="text-muted" >Privacy</a></li>
                                <li><a href="/" className="text-muted" >Terms</a></li>
                            </ul>
                        </div>
                    </div>
                    <a href="/"  className="backtotop" > <span className="fa fa-hand-o-up fa-3x text-dark" aria-hidden="true"></span> </a>

                </div>

            </footer>
        </div>
    )
}

export default Footer
