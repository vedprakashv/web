import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './styles.css';
import { FaUser } from 'react-icons/fa'
import { useHistory } from 'react-router-dom';


const Signin = () => {
    const history = useHistory();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');


    const loginUser = async (e) => {
        e.preventDefault();
        const res = await fetch("/signin", {
            method: "POST",
            headers: { 
                "Content-Type": "application/json" 
            },
            body: JSON.stringify({ email, password })
        });
        const data = res.json();    
        if(!email || !password){
            window.alert("Fill all data");
            console.log("Fill all data ");
        }
        else if(data.status === 422 || !data)
        {
            window.alert("Invalid SIGNIN ");
            console.log("Invalid SIGNIN ");
        }
        else{
            window.alert("SIGNIN is success");
            console.log("SIGNIN is success");
            history.push("/aboutme")
        }
    }

    return (
        <>
            <section className="frame">
                <div className="bg-circle1 bg-circle"></div>
                <div className="bg-circle1_1 bg-circle"></div>
                <div className="sm-circle1 bg-circle"></div>
                <div className="bg-circle2 bg-circle"></div>
                <div className="sm-circle2 bg-circle"></div>

                <div className="">
                    <div className="signin-frame ">
                        <form method="POST" className="container40 m-auto">
                                <div className="signin-user-icon text-center"> <FaUser size={80} /> </div>
                                <h1 className="display-6 pb-3 text-light fw-normal text-center">Login</h1>
                                <input type="email" name="email" id="email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        className="form-control form-group"
                                        placeholder="E-mail" />
                                <br />
                                
                                    <input type="password" name="password" id="password"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        className="form-control form-group"
                                        placeholder="Password" />
                                

                                {/* <div className="checkbox text-light  mb-3 mt-2">
                                    <label><input type="checkbox" value="remember-me" /> Remember me  </label>
                                    <label><input type="checkbox" value="remember-me" /> Login as Admin </label>
                                </div> */}
                                
                                <input
                                    className="signin-btn form-button form-group w-100 login-btn btn btn-lg container "
                                    id="login"
                                    name="login"
                                    onClick={loginUser}
                                    type="submit" value="Log In" />
                               

                                <a href="/"><p className="pt-5 pb-1 text-muted">Forgot Password</p></a>
                                <a href="/Signup"><p className="pt-1 text-muted">Create an Account</p></a>
                            

                        </form>
                    </div>

                </div>
                <div className="log-frame_1 container"></div>
            </section>
        </>
    )
}

export default Signin
