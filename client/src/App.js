import React from 'react';
import { Route } from 'react-router-dom';
import './App.css';

import Home from './components/Home';
import Navbar from './components/Navbar';
import About from './components/About';
import Courses from './components/Courses';
import Signin from './components/Signin';
import Signup from './components/Signup';
import Footer from './components/Footer';
import Aboutme from './components/Aboutme';

import Html from './courses/Html';

const App = () => {
  return (
    <>
      <Navbar />

        <Route exact path='/'>
          <Home />
        </Route>
        <Route path='/Courses'>
          <Courses />
        </Route>
        <Route path='/Html'> 
          <Html />
        </Route>
        <Route path='/About'>
          <About />
        </Route>
        <Route path='/Signin'>
          <Signin />
        </Route>
        <Route path='/Signup'>
          <Signup />
        </Route>
        <Route path='/Aboutme'>
          <Aboutme />
        </Route>

      <Footer />
    </>
  )
}

export default App;
